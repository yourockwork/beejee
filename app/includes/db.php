<?php

class DB
{
	protected static $connection;

	private function __construct()
	{
	}

	private function __clone()
	{
	}

	public static function connect($host = DB_HOST, $user = DB_LOGIN, $password = DB_PASSWORD, $db_name = DB_NAME)
	{
		if (empty(self::$connection)) {
			self::$connection = new mysqli($host, $user, $password, $db_name);
		}

		self::query("SET NAMES UTF8");

		if (!self::$connection) {
			throw new Exception('Could not connect to DB ');
		}
	}

	protected function prepare($sql){
		if (!self::$connection) {
			return false;
		}

		$result = self::$connection->query($sql);

		if (self::$connection->error) {
			throw new Exception(self::$connection->error);
		}

		if (is_bool($result)) {
			return $result;
		}
	}

	public static function numRows($sql){
		if (!self::$connection) {
			return false;
		}

		$result = self::$connection->query($sql);

		if (self::$connection->error) {
			throw new Exception(self::$connection->error);
		}

		if (is_bool($result)) {
			return $result;
		}

		$numRows = $result->num_rows;

		$result->free_result(); // результаты запроса нам больше не нужны, чистим память

		return $numRows;
	}

	public static function query($sql)
	{

		if (!self::$connection) {
			return false;
		}

		$result = self::$connection->query($sql);

		if (self::$connection->error) {
			throw new Exception(self::$connection->error);
		}

		if (is_bool($result)) {
			return $result;
		}

		$data = array();

		while ($row = $result->fetch_assoc()) {
			$data[] = $row;
		}

		$result->free_result(); // результаты запроса нам больше не нужны, чистим память

		return $data;
	}

	public static function last_id()
	{
		if (!self::$connection) {
			return false;
		}

		$result = self::$connection->insert_id;

		if (self::$connection->error) {
			throw new Exception(self::$connection->error);
		}

		if (is_int($result)) {
			return $result;
		} else {
			return false;
		}
	}

	public static function escape($str)
	{
		return self::$connection->escape_string($str);
	}
}