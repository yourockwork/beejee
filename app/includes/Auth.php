<?php

class Auth
{
	public function enter()
	{
		$error = array(); //массив для ошибок
		if ($_POST['login'] != "" && $_POST['password'] != "") //если поля заполнены
		{
			$login = DB::escape($_POST['login']);
			$password = DB::escape($_POST['password']);
			$row = DB::query("SELECT * FROM admins WHERE login = '".$login."'");

			if (count($row) == 1) //если нашлась одна строка, значит такой юзер существует в базе данных
			{
				$row = $row[0];
				if (md5(md5($password).$row['salt']) == $row['password']) //сравнивается хэшированный пароль из базы данных с хэшированными паролем, введённым пользователем
				{
					//пишутся логин и хэшированный пароль в cookie, также создаётся переменная сессии
					setcookie("login", $row['login'], time() + 50000);
					setcookie("password", md5($row['login'] . $row['password']), time() + 50000);
					$_SESSION['id'] = $row['id'];   //записываем в сессию id пользователя

					return $error;
				} else //если пароли не совпали
				{
					$error[] = "Неверный пароль";
					return $error;
				}
			} else //если такого пользователя не найдено в базе данных
			{
				$error[] = "Неверный логин и пароль";
				return $error;
			}
		} else {
			$error[] = "Поля не должны быть пустыми!";
			return $error;
		}
	}

	public function login()
	{
		session_start();
		if (isset($_SESSION['id']))	//если сесcия есть
		{
			if (isset($_COOKIE['login']) && isset($_COOKIE['password'])) //если cookie есть, обновляется время их жизни и возвращается true
			{
				SetCookie("login", "", time() - 1, '/');
				SetCookie("password", "", time() - 1, '/');
				setcookie("login", $_COOKIE['login'], time() + 50000, '/');
				setcookie("password", $_COOKIE['password'], time() + 50000, '/');

				return true;
			}
			else //иначе добавляются cookie с логином и паролем, чтобы после перезапуска браузера сессия не слетала
			{
				$id = $_SESSION['id'];
				$row = DB::query("select * from admins where id='".$id."'");
				if (count($row) == 1) //если получена одна строка
				{
					$row = $row[0];
					setcookie("login", $row['login'], time() + 50000, '/');

					setcookie("password", md5($row['login'] . $row['password']), time() + 50000, '/');

					return true;
				}
				else return false;
			}
		}
		else //если сессии нет, проверяется существование cookie. Если они существуют, проверяется их валидность по базе данных
		{
			if (isset($_COOKIE['login']) && isset($_COOKIE['password'])) //если куки существуют
			{
				$login = DB::escape($_COOKIE['login']);
				$row = DB::query("SELECT * FROM admins WHERE login = '".$login."'");

				if (count($row) == 1 && md5($row[0]['login'] . $row[0]['password']) == $_COOKIE['password']) //если логин и пароль нашлись в базе данных
				{
					$_SESSION['id'] = $row[0]['id']; //записываем в сесиию id

					return true;
				}
				else //если данные из cookie не подошли, эти куки удаляются
				{
					SetCookie("login", "", time() - 360000, '/');
					SetCookie("password", "", time() - 360000, '/');
					return false;

				}
			}
			else //если куки не существуют
			{
				return false;
			}
		}
	}

	public function out () {
		session_start();
		unset($_SESSION['id']); //удалятся переменная сессии
		SetCookie("login", ""); //удаляются cookie с логином
		SetCookie("password", ""); //удаляются cookie с паролем
		//header('Location: http://'.$_SERVER['HTTP_HOST'].'/'); //перенаправление на главную страницу сайта
	}
}