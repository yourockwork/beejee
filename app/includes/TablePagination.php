<?php

class TablePagination
{
	/**
	 * Текущая страница
	 */
	private $currentPage;

	/**
	 * Общее количество записей в табилце
	 */
	private $totalRecords;

	/**
	 * Общее количество страниц
	 */
	private $totalPages;

	/**
	 * Число записей выводимых на станице
	 */
	private $number;

	public function __construct($currentPage = 1, $number = 3, $totalRecords)
	{
		$this->number = $number;
		$this->totalRecords = $totalRecords;
		$this->totalPages = intval(($this->totalRecords - 1) / $this->number) + 1;

		if (empty($currentPage) or $currentPage < 0){
			$this->currentPage = 1;
		}
		elseif ($currentPage > $this->totalPages){
			$this->currentPage = $this->totalPages;
		}
		else{
			$this->currentPage = $currentPage;
		}
	}

	public function getTotalPages(){
		return $this->totalPages;
	}

	public function getStartIndex(){
		return (($this->currentPage * $this->number) - $this->number);
	}

	public function getNumber(){
		return $this->number;
	}

	public function getCurrentPage(){
		return $this->currentPage;
	}
}