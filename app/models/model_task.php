<?php

class Model_task extends Model
{
	public function get_data(){
		try{
			return DB::query('select * from task');
		}
		catch (Exception $e){
			echo $e->getMessage();
		}
	}

	public function getDataJSON($currentPage = 1, $number = 3, $totalRecords, $col, $sort){
		try{
			$pagination = new TablePagination($currentPage, $number, $totalRecords);
			$start = DB::escape($pagination->getStartIndex());
			$end = DB::escape($pagination->getNumber());
			//	внедрить `белые списки`
			$col = DB::escape($col);
			$sort = DB::escape($sort);

			$result['totalPages'] = $pagination->getTotalPages();
			$result['items'] = DB::query("select * from task order by $col $sort limit $start, $end");

			echo json_encode($result);
			exit();
		}
		catch (Exception $e){
			echo $e->getMessage();
		}
	}

	public function getLimitData($start, $end){
		try{
			$start = DB::escape($start);
			$end = DB::escape($end);

			return DB::query("select * from task limit $start, $end");
		}
		catch (Exception $e){
			echo $e->getMessage();
		}
	}

	public function getCount(){
		return DB::numRows('select * from task');
	}

	public function add(){
		try {
			// по умолчанию задачи не выполнены и не отредактированы администратором
			DB::query('insert into task (user_name, email, description) 
					VALUES (
						"'.DB::escape($_POST['username']).'",
						"'.DB::escape($_POST['email']).'",
						"'.DB::escape($_POST['description']).'"
					)
		  	');

			$result = DB::query('select * from task where id="'. DB::last_id() .'"');

			echo json_encode($result);
			exit();
		}
		catch (Exception $e) {
			echo $e->getMessage();
		}
	}

	public function update(){
		try {
			$res = DB::query('update task set modified="1", description="'.DB::escape($_POST['updatetxt']).'" where id="'.DB::escape($_POST['updateid']).'"');
			echo json_encode($res);
			exit();
		}
		catch (Exception $e) {
			echo $e->getMessage();
		}
	}

	public function updateStatus(){
		try {
			$res = DB::query('update task set status="'.DB::escape($_POST['updatetxt']).'" where id="'.DB::escape($_POST['updateid']).'"');
			echo json_encode($res);
			exit();
		}
		catch (Exception $e) {
			echo $e->getMessage();
		}
	}

	/*public function remove(){
		try {
			DB::query('delete from task where id="'.DB::escape($_POST['remove']).'"');
		}
		catch (Exception $e) {
			echo $e->getMessage();
		}
	}*/

}