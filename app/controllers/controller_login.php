<?php

class Controller_login extends Controller
{
	private $uid;
	private $msg = '';

	public function __construct()
	{
		$this->view = new View();
		$this->auth = new Auth();
	}

	public function action_index()
	{
		$this->login();
		$data['err'] = $this->msg;
		$data['uid'] = $this->uid;

		$this->view->twig('', 'template_login.html', $data);
		//header('Location: http://'.$_SERVER['HTTP_HOST'].'/task'); //перенаправление
	}

	public function login(){
		if ($this->auth->login()) //вызываем функцию login, которая определяет, авторизирован пользователь или нет
		{
			$this->uid = $_SESSION['id']; //если пользователь авторизирован, присваиваем переменной $UID его id
			$this->msg = 'Вы авторизованы';
		}
		else //если пользователь не авторизирован, проверяем, была ли нажата кнопка входа на сайт
		{
			if(isset($_POST['log_in']))
			{
				$error = $this->auth->enter(); //функция входа на сайт

				if (count($error) == 0) //если ошибки отсутствуют, авторизируем пользователя
				{
					$this->uid = $_SESSION['id'];
					$this->msg = 'Вы авторизованы';
				}
				else{
					$this->msg = $error[0];
				}
			}
		}
	}
}