<?php

class Controller_logout extends Controller
{
	public function __construct()
	{
		$this->view = new View();
		$this->auth = new Auth();
	}

	public function action_index(){
		$this->auth->out();

		$data['err'] = 'Вы не авторизованы';
		$this->view->twig('', 'template_login.html', $data);
	}
}