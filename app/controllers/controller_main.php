<?php

class Controller_main extends Controller
{
	private $uid;

	public function __construct()
	{
		$this->view = new View();
		$this->auth = new Auth();
	}

	public function action_index()
	{
		$this->login();
		$data['uid'] = $this->uid;

		$this->view->twig('', 'template_main.html', $data);
	}

	public function login(){
		if ($this->auth->login()) //вызываем функцию login, которая определяет, авторизирован пользователь или нет
		{
			$this->uid = $_SESSION['id']; //если пользователь авторизирован, присваиваем переменной $UID его id
		}
		else //если пользователь не авторизирован, проверяем, была ли нажата кнопка входа на сайт
		{
			if(isset($_POST['log_in']))
			{
				$error = $this->auth->enter(); //функция входа на сайт

				if (count($error) == 0) //если ошибки отсутствуют, авторизируем пользователя
				{
					$this->uid = $_SESSION['id'];
				}
				else{
					echo $error[0];
				}
			}
		}
	}
}