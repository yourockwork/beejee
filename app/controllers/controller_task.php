<?php

class Controller_task extends Controller
{
	private $uid;

	public function __construct()
	{
		$this->model = new Model_task();
		$this->view = new View();
		$this->auth = new Auth();
	}

	public function action_index()
	{
		/* обработка ajax запросов */
		if(! empty($_POST["action"]))
		{
			switch($_POST["action"])
			{
				case 'add':
					return $this->model->add();
					break;
				case 'sort':
					return $this->model->getDataJSON($_POST['active'], 3, $this->model->getCount(), $_POST['col'], $_POST['sort']);
					break;
				case 'pagination':
					return $this->model->getDataJSON($_POST['active'], 3, $this->model->getCount(), $_POST['col'], $_POST['sort']);
					break;
				case 'checkLogin':
					return $this->checkLogin();
					break;
			}
		}

		$this->login();

		/**
		 * Действия только для авторизированных пользователей
		 */
		if ($this->uid){
			/* обработка ajax запросов */
			if(! empty($_POST["action"]))
			{
				switch($_POST["action"])
				{
					/*case 'remove':
						return $this->model->remove();
						break;*/
					case 'update':
						return $this->model->update();
						break;
					case 'updateStatus':
						return $this->model->updateStatus();
						break;
				}
			}
		}

		$pagination = new TablePagination(1,3, $this->model->getCount());
		$data['items'] = $this->model->getLimitData($pagination->getStartIndex(), $pagination->getNumber());
		$data['currentPage'] = $pagination->getCurrentPage();
		$data['totalPages'] = $pagination->getTotalPages();
		$data['uid'] = $this->uid;

		$this->view->twig('', 'template_task.html', $data);
	}

	public function login(){
		if ($this->auth->login()) //вызываем функцию login, которая определяет, авторизирован пользователь или нет
		{
			$this->uid = $_SESSION['id']; //если пользователь авторизирован, присваиваем переменной $this->uid его id
		}
		else //если пользователь не авторизирован, проверяем, была ли нажата кнопка входа на сайт
		{
			if(isset($_POST['log_in']))
			{
				$error = $this->auth->enter(); //функция входа на сайт

				if (count($error) == 0) //если ошибки отсутствуют, авторизируем пользователя
				{
					$this->uid = $_SESSION['id'];
				}
			}
		}
	}

	public function checkLogin(){
		echo json_encode($this->auth->login());
		exit();
	}
}