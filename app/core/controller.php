<?php

class Controller {
	public $model;
	public $view;
	public $auth;

	public function __construct()
	{
		$this->view = new View();
	}

	// действие по умолчанию
	public function action_index(){}
}