<?php

class Model
{
	/**
	 * Выборка данных.
	 * Во всех дочерних моделях мы переопределяем эти методы
	 */
	public function get_data(){}
	public function getDataJSON(){}


	/**
	 * Выборка данных для пагинации.
	 */
	public function getLimitData($start, $end){}
}