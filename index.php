<?php
/**
 * Точка входа в приложение
 */

//ini_set('display_errors', 1);

ini_set("session.use_trans_sid", true);

require_once 'config.php';
require_once 'app/includes/db.php'; // класс работы с базой данных
require_once 'app/includes/TablePagination.php'; // класс работы с базой данных
require_once 'app/includes/Auth.php';
require_once 'app/core/model.php';  // ядро модели
require_once 'app/core/view.php';   // ядро представления
require_once 'app/core/controller.php'; // ядро контроллера
require_once 'app/core/route.php';  // класс маршрутизации
require_once 'app/includes/Twig/Autoloader.php'; // шаблонизатор twig

DB::connect();
Route::start();