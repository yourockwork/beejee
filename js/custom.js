$( document ).ready(function() {

    function tbl__line(val) {

        let html =
            '<div class="js-tbl__line tbl__line">'+
            '<div class="tbl__cell ">' +
            '<div class="d-inline-block hidden-lg hidden-md">' +
            '<b>' +
            '№' +
            '</b>' +
            '</div>' +
            val.id +
            '</div>' +
            '<div class="tbl__cell">' +
            '<div class="d-inline-block hidden-lg hidden-md mb-5px">' +
            '<b>' +
            'Имя пользователя: ' +
            '</b>' +
            '</div>' +
            val.user_name +
            '</div>' +
            '<div class="tbl__cell">' +
            '<div class="d-inline-block hidden-lg hidden-md mb-5px">' +
            '<b>' +
            'Email: ' +
            '</b>' +
            '</div>' +
            val.email+
            '</div>' +
            '<div class="tbl__cell">' +
            '<form class="js-form-update" method="post">' +
            '<div class="d-inline-block hidden-lg hidden-md mb-5px">' +
            '<b>' +
            'Текст задачи: ' +
            '</b>' +
            '</div>' +
            '<input type="hidden" name="updateid" value="'+val.id+'">' +
            '<input class="js-form-edit form-edit" name="updatetxt" value="'+val.description+'" placeholder="введите текст">' +
            '</form>' +
            '</div>' +
            '<div class="tbl__cell">' +
            '<div class="d-inline-block hidden-lg hidden-md mb-5px">' +
            '<b>' +
            'Статус: ' +
            '</b>' +
            '</div>' +
             val.status+
            '</div>' +
            /*
            '<div class="tbl__cell">' +
            '<form class="js-form-remove" method="post">' +
            '<input type="hidden" name="remove" value="'+val.id+'">' +
            '<button class="js-btn-remove btn-remove">' +
            '<span class="glyphicon glyphicon-remove">' +
            '</span>' +
            '</button>' +
            '</form>' +
            '</div>' +
            */
            '</div>';
        return html;
    };

    function tbl__body__admin(val) {
        let statuss = '';
        let modified = '';

        if (val.modified == '1'){
            modified = 'g-modified'
        }

        if (val.status == 'true'){
            statuss = '<input class="js-update-status" type="checkbox" checked="checked">';
        }
        else {
            statuss = '<input class="js-update-status" type="checkbox">';
        }

        let html =
            '<div class="js-tbl__line tbl__line '+modified+'">'+
                '<div class="tbl__cell">' +
                    '<div class="d-inline-block hidden-lg hidden-md">' +
                        '<b>' +
                        '№' +
                        '</b>' +
                    '</div>' +
                    val.id +
                '</div>' +
                '<div class="tbl__cell">' +
                    '<div class="d-inline-block hidden-lg hidden-md mb-5px">' +
                        '<b>' +
                        'Имя пользователя: ' +
                        '</b>' +
                    '</div>' +
                     val.user_name+
                    '</div>' +
                    '<div class="tbl__cell">' +
                        '<div class="d-inline-block hidden-lg hidden-md mb-5px">' +
                        '<b>' +
                        'Email: ' +
                        '</b>' +
                    '</div>' +
                     val.email+
                '</div>' +
                '<div class="tbl__cell">' +
                    '<form class="js-form-update">' +
                        '<div class="d-inline-block hidden-lg hidden-md mb-5px">' +
                            '<b>' +
                            'Текст задачи: ' +
                            '</b>' +
                        '</div>' +
                        '<input type="hidden" name="updateid" value="'+val.id+'">' +
                        '<input class="js-form-edit form-edit" name="updatetxt" value="'+val.description+'" placeholder="введите текст">' +
                    '</form>' +
                '</div>' +
                '<div class="tbl__cell">' +
                    '<form class="js-form-update" method="post">' +
                        '<div class="b-label d-inline-block hidden-lg hidden-md mb-5px">' +
                            '<b>' +
                                'Статус: ' +
                            '</b>' +
                        '</div>' +
                        '<input type="hidden" name="updateid" value="' + val.id + '">'+
                        statuss +
                    '</form>' +
                '</div>' +
                /*
                '<div class="tbl__cell">' +
                    '<form class="js-form-remove" method="post">' +
                        '<input type="hidden" name="remove" value="'+val.id+'">' +
                        '<button class="js-btn-remove btn-remove">' +
                            '<span class="glyphicon glyphicon-remove">' +
                            '</span>' +
                        '</button>' +
                    '</form>' +
                '</div>' +
                */
            '</div>'
        return html;
    };

    function tbl__body__user(val) {
        let statuss = '';
        let modified = '';

        if (val.modified == '1'){
            modified = 'g-modified'
        }

        if (val.status == 'true'){
            statuss = 'Выполнено';
        }
        else {
            statuss = 'Не выполнено';
        }

        let html =
            '<div class="js-tbl__line tbl__line '+modified+'">' +
            '<div class="tbl__cell">' +
            '<div class="d-inline-block hidden-lg hidden-md">' +
            '<b>' +
            '№' +
            '</b>' +
            '</div>' +
            val.id +
            '</div>' +
            '<div class="tbl__cell">' +
            '<div class="d-inline-block hidden-lg hidden-md mb-5px">' +
            '<b>' +
            'Имя пользователя: ' +
            '</b>' +
            '</div>' +
                val.user_name+
            '</div>' +
            '<div class="tbl__cell">' +
                '<div class="d-inline-block hidden-lg hidden-md mb-5px">' +
                    '<b>' +
                    'Email: ' +
                    '</b>' +
                '</div>' +
                val.email+
            '</div>' +
            '<div class="tbl__cell">' +
                '<div class="d-inline-block hidden-lg hidden-md mb-5px">' +
                    '<b>' +
                    'Текст задачи: ' +
                    '</b>' +
                '</div>' +
                val.description +
            '</div>' +
            '<div class="tbl__cell">' +
                '<div class="d-inline-block hidden-lg hidden-md mb-5px">' +
                    '<b>' +
                    'Статус: ' +
                    '</b>' +
                '</div>' +
                '<div class="b-label d-inline-block">' +
                    statuss +
                '</div>' +
            '</div>' +
            /*
            '<div class="tbl__cell">' +
                '<form class="js-form-remove" method="post">' +
                    '<input type="hidden" name="remove" value="'+val.id+'">' +
                    '<button class="js-btn-remove btn-remove">' +
                        '<span class="glyphicon glyphicon-remove">' +
                        '</span>' +
                    '</button>' +
                '</form>' +
            '</div>' +
            */
            '</div>'
        return html;
    };

    function pagination(totalPages, page = 1) {
        let html = '';

        for (let i = 1; i <= totalPages; i++){
            if (page === i){
                html += `<li class="active"><a data-page="${i}">${i}</a></li>`;
            }
            else {
                html += `<li ><a data-page="${i}">${i}</a></li>`;
            }
        }

        return html;
    }

    $('.js-records').on('submit','.js-form-add', function () {
        let $this = $(this);
        let valid = true;
        let $username = $this.find('input[name="username"]');
        let $email = $this.find('input[name="email"]');
        let $description = $this.find('textarea[name="description"]');

        $username.removeClass('js-form-edit--err');
        $email.removeClass('js-form-edit--err');
        $description.removeClass('js-form-edit--err');

        if(!$username.val() || $username.val() == ''){
            $username.attr('placeholder','Вы не ввели имя');
            $username.addClass('js-form-edit--err');
            valid = false;
        }

        if(!$email.val() || $email.val() == ''){
            $email.attr('placeholder','Вы не ввели email');
            $email.addClass('js-form-edit--err');
            valid = false;
        }

        if(!$description.val() || $description.val() == ''){
            $description.attr('placeholder','Вы не ввели описание');
            $description.addClass('js-form-edit--err');
            valid = false;
        }

        if (valid) {
            $.ajax({
                datatype: 'json',
                type : 'POST',
                data: {
                    action : 'add',
                    username : $username.val(),
                    email : $email.val(),
                    description : $description.val()
                }
            })
            location.reload();
            return false; // отменяем отправку формы
        }
        else {
            return false;
        }
    });

    $('.js-records').on('click','.js-sort-col', function(){
        let $this = $(this);
        $this.parents('.js-sort').find('.dropdown-menu li').removeClass('active');
        $this.parent().addClass('active');
        let col = $this.parents('.js-records').find('.js-sort li.active a').data('col');
        let sort = $this.parents('.js-records').find('.js-sort li.active a').data('sort');
        let active = $this.parents('.js-records').find('.js-tbl-pagination li.active a').data('page');
        let login = false;

        $.ajax({
            datatype: 'json',
            type : 'POST',
            data: {
                action : 'checkLogin',
            }
        })
        .done(function(data) {
            login = JSON.parse(data);

            $.ajax({
                datatype: 'json',
                type : 'POST',
                data: {
                    action : 'sort',
                    col: col,
                    sort: sort,
                    active: active
                }
            })
            .done(function(data) {
                    let arr = JSON.parse(data);

                    $this.parents('.js-records').find('.js-tbl__line').remove();

                    if(login == true){
                        $.each(arr['items'], function(index, val){
                            $this.parents('.js-records').find('.js-tbl_body').append(
                                tbl__body__admin(val)
                            );
                        });
                    }
                    else{
                        $.each(arr['items'], function(index, val){
                            $this.parents('.js-records').find('.js-tbl_body').append(
                                tbl__body__user(val)
                            );
                        });
                    }

                    $this.parents('.js-records').find('.js-tbl-pagination li').remove();
                    $('.js-tbl-pagination').append(
                        pagination(arr['totalPages'], active)
                    );

            });
        })

        return false;
    });

    $(".js-records").on('click', '.js-tbl-pagination li a', function(event) {
        // отменяем переход по ссылке
        event.preventDefault();
        let $this = $(this);
        let page = $this.data('page');
        let col = $this.parents('.js-records').find('.js-sort li.active a').data('col');
        let sort = $this.parents('.js-records').find('.js-sort li.active a').data('sort');
        let login = false;

        $.ajax({
            datatype: 'json',
            type : 'POST',
            data: {
                action : 'checkLogin',
            }
        })
        .done(function(data) {
            login = JSON.parse(data);
            $.ajax({
                datatype: 'json',
                type : 'POST',
                data: {
                    action : 'pagination',
                    col: col,
                    sort: sort,
                    active: page,
                }
            })
            .done(function(data) {
                    let arr = JSON.parse(data);

                    $this.parents('.js-records').find('.js-tbl__line').remove();

                    if(login == true){
                        $.each(arr['items'], function(index, val){
                            $this.parents('.js-records').find('.js-tbl_body').append(
                                tbl__body__admin(val)
                            );
                        });
                    }
                    else{
                        $.each(arr['items'], function(index, val){
                            $this.parents('.js-records').find('.js-tbl_body').append(
                                tbl__body__user(val)
                            );
                        });
                    }

                    $this.parents('.js-records').find('.js-tbl-pagination li').remove();
                    $('.js-tbl-pagination').append(
                        pagination(arr['totalPages'], $this.data('page'))
                    );
                });
        })

        return false;
    });

    $('.js-records').on('focus','.js-form-update', function () {
        let $this=$(this);
        let $updatetxt=$this.find('input[name="updatetxt"]');
        $updatetxt.removeClass('js-form-edit--err');
        $updatetxt.attr('placeholder','введите текст');
        $updatetxt.addClass('js-form-edit--change');
    });

    $('.js-records').on('blur','.js-form-update', function () {
        let $this=$(this);
        let $updateid = $this.find('input[name="updateid"]');
        let $updatetxt = $this.find('input[name="updatetxt"]');
        $updatetxt .removeClass('js-form-edit--change');
        if(!$updatetxt.val() || $updatetxt.val() == '' || !$updateid.val() || $updateid.val() == ''){
            $updatetxt.attr('placeholder','Введите текст');
            $updatetxt.addClass('js-form-edit--err');
        }
        else{
            $.ajax({
                datatype: 'json',
                type : 'POST',
                data: {
                    action : 'update',
                    updateid : $updateid.val(),
                    updatetxt : $updatetxt.val(),
                }
            })
            .done(function() {
                $updatetxt .addClass('js-form-edit--success');
            });
        }
        return false; // отменяем перезагрузку страницы
    });

    $('.js-records').on('mousedown','.js-update-status', function () {
        let $this = $(this);
        let $updateid = $this.parent().find('input[name="updateid"]').val();
        let $status = !$this.is(':checked');

        $.ajax({
            datatype: 'json',
            type : 'POST',
            data: {
                action : 'updateStatus',
                updateid : $updateid,
                updatetxt : $status,
            }
        })
        .done(function() {

        });

    });

    $('.js-records').on('submit','.js-form-update', function () {
        let $this=$(this);
        let $updateid=$this.find('input[name="updateid"]');
        let $updatetxt=$this.find('input[name="updatetxt"]');
        $updatetxt.removeClass('js-form-edit--change');
        if(!$updatetxt.val() || $updatetxt.val() == '' || !$updateid.val() || $updateid.val() == '') {
            $updatetxt.attr('placeholder','Введите текст');
            $updatetxt.removeClass('js-form-edit--success');
            $updatetxt.addClass('js-form-edit--err');
        }
        else {
            $.ajax({
                datatype: 'json',
                type : 'POST',
                data: {
                    action : 'update',
                    updateid : $updateid.val(),
                    updatetxt : $updatetxt.val(),
                }
            })
            .done(function(data) {
                $updatetxt.addClass('js-form-edit--success');
            });
        }
        return false; // отменяем перезагрузку страницы
    });

    /*$('.js-records').on('submit','.js-form-remove', function () {
        let $this = $(this);
        $.ajax({
            datatype: 'json',
            type : 'POST',
            data: {
                action : 'remove',
                remove : $(this).find('input[name="remove"]').val()
            }
        })
        .done(function() {
            $this.parents('.js-tbl__line').remove();
        });
        return false; // отменяем перезагрузку страницы
    });*/

});